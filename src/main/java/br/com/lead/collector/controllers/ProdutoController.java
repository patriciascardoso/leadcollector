package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.awt.color.ProfileDataException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto){
       Produto objetoProduto = produtoService.salvarProduto(produto);
       return objetoProduto;
    }

    @GetMapping
    public Iterable<Produto> lerTodosProdutos (){
        return produtoService.lerTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto pesquisaPorId(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoService.buscarProdutoPeloId(id);
            return produto;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public Produto atualizarProduto (@RequestBody @Valid Produto produto, @PathVariable(name = "id") int id){
        try{
            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return  produtoDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void deletarProduto (@PathVariable(name = "id") int id){
        try{
            produtoService.deletarProduto(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
