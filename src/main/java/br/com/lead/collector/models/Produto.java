package br.com.lead.collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank (message = "Nome não pode estar em branco")
    @NotNull (message = "Nome não pode ser nullo")
    private String nome;

    @NotNull (message = "Descrição não pode ser nullo")
    @NotBlank (message = "Descrição não pode ser em branco")
    private String descricao;

    @NotNull (message = "Preço não pode ser nullo")
    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin(value = "1.0", message = "Valor deve ser no minimo de 1 real")
    private double preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
